(function(angular) {
    var userController = function($scope, UserService) {
        UserService.get(1, function(data) {
            $scope.user = data;
        });
        UserService.getObjectivesFor(1, function(data) {
            $scope.objectives = data;
        });
    };
    var objectiveController = function($scope, $routeParams, $timeout, UserService) {
        var ownerId = undefined;
        UserService.getObjective($routeParams.objectiveId, function(objective) {
            $scope.objective = objective;
            ownerId = objective.owner;
        });
        $scope.isFriend = function(milestone) {
            return milestone.author.id !== ownerId;
        };
        $scope.submitComment = function(milestone, comment) {
            milestone.comments.push({
                                        "message": comment,
                                        "author": {
                                            "id": 1,
                                            "firstName": "Sami",
                                            "lastName": "Smith"
                                        },
                                        "date": "2013-02-17"
                                    });
            milestone.nextComment = '';
        };
        $scope.addLike = function(milestone) {
            milestone.likeCount += 1;
        };
        $timeout(function() {
            $scope.objective.milestones.push({
                                                 "url": "http://25.media.tumblr.com/bd641b4de0fcb245c959a15138045c61/tumblr_mgcruuQWWO1s2kox0o1_400.gif",
                                                 "author": {
                                                     "id": 2,
                                                     "firstName": "Mimi",
                                                     "lastName": "Canscanza"
                                                 },
                                                 "likeCount": 0,
                                                 "comments": []
                                             })
        }, 30000);
    };
    angular.
        module('UserPageController', ['UserServiceModule']).
        controller('UserController', ['$scope', 'UserService', userController]).
        controller('ObjectiveController', ['$scope', '$routeParams', '$timeout', 'UserService', objectiveController]);
}(angular));
