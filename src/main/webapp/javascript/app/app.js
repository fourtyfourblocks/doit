(function(angular) {
    var router = function($routeProvider) {
      $routeProvider.when('/user/:userId', {templateUrl: '/templates/user.html', controller: 'UserController'})
        $routeProvider.when('/objective/:objectiveId',
                            {templateUrl: '/templates/objective.html', controller: 'ObjectiveController'})
        $routeProvider.otherwise({redirectTo: '/user/1'});
      };
    angular.
        module('Main', ['ui', 'UserPageController']).
        config(['$routeProvider', router]);
} (angular));
