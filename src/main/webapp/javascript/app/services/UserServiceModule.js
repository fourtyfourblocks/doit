/**
 * @author : pawel kaminski
 * @since : 16.02.2013 17:01
 *
 * description :
 */
(function(angular) {
    'use strict';
    angular.xAdd = function(list, when) {
        var results = [];
        angular.forEach(list, function(value, key) {
            if (when(value, key)) {
                results.push(value);
            }
        });
        return results;
    };
    var localData = undefined;
    var getCacheData = function(source, handler) {
        if (localData) {
            handler(localData);
        } else {
            source.get({}, function(data) {
                localData = data;
                handler(data);
            });
        }
    };
    var userService = function(logger, restResource) {
        var userData = restResource('/data/users.json', {}, {});
        var localObjectivesCounter = 21


        return {
            "get" : function(id, ok) {
                getCacheData(userData, function(data) {
                    ok(data.users[id-1]);
                });
            },
            "getFriends": function(id, ok) {
                getCacheData(userData, function(data) {
                    ok(angular.xAdd(data, function(user) {
                        return user.id != id;
                    }));
                });
            },
            "getObjectivesFor": function(ownerId, ok) {
                getCacheData(userData, function(data) {
                    ok(angular.xAdd(data.objectives, function(objective) {
                        return objective.owner === ownerId;
                    }));
                })
            },
            "getObjective": function(id, ok) {
                getCacheData(userData, function(data) {
                    ok(data.objectives[id - 21]);
                });
            },
            "addObjective": function(objective, user) {
                getCacheData(userData, function(data) {
                    localObjectiveCounter += 1

                    objective.id = localObjectiveCounter;
                    objective.startDate = '2013/02/17';
                    objective.owner = user.id;
                    objective.accessList = [];
                    objective.accessList.push(user.id);
                    data.objectives.push(objective);
                });


            }
        };
    };

    angular.
        module('UserServiceModule', ['ngResource']).
        factory('UserService', ['$log', '$resource', userService]);
}(angular));